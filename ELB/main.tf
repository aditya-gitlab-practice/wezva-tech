provider "aws" {
  region = "ap-south-1"
}

data "aws_availability_zones" "all" {}

resource "aws_security_group" "elb" {
  name = "security-zone-elb-aug2023"
  
  egress {
    from_port = 0
	to_port = 0
	protocol = "-1"
	cidr_blocks = ["0.0.0.0/0"]
  } 
  
  ingress {
    from_port = var.elb
	to_port = var.elb
	protocol = "tcp"
	cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "instance" {
  name = "security-group-instance-aug2023"
  
  ingress {
    from_port = var.server
	to_port = var.server
	protocol = "tcp"
	cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "example"{
  name = "aws-elb-examplee-aug2023"
  availability_zones = data.aws_availability_zones.all.names
  security_groups = [aws_security_group.elb.id]

  health_check {
	target = "HTTP:${var.server}/"
	interval = 30
	timeout = 3
	healthy_threshold = 2
	unhealthy_threshold = 2
  } 
  
  listener {
    lb_port = var.elb
	lb_protocol = "http"
    instance_port = var.server
	instance_protocol = "http"
  }
}


resource "aws_launch_configuration" "example" {
  name = "launch-config-example-aug202"
  
  image_id = "ami-08e5424edfe926b43"
  instance_type = "t2.large"
  security_groups = [aws_security_group.instance.id]
  
  user_data = <<-EOF
              #!/bin/bash
              echo '<html><body><h1 style="font-size:50px;color:blue;">KING <br> <font style="color:red;"> www.wezva.com <br> <font style="color:green;"> +91-12345 </h1> </body></html>' > index.html
              nohup busybox httpd -f -p "${var.server}" &
              EOF

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "example" {
  name = "auto-scale-exampl-aug202"
  launch_configuration = aws_launch_configuration.example.id
  availability_zones = data.aws_availability_zones.all.names
  
  min_size = 2
  max_size = 10
  
  load_balancers = [aws_elb.example.name]
  health_check_type = "ELB"

  tag {
    key                 = "Name"
    value               = "ADAM-ASG-PROJECT"
    propagate_at_launch = true
  }
}
