provider "aws" {
  region = "ap-south-1"
}

variable "myhosts" {
  type = map
  default = {
     Jenkins-Master = "t2.micro" 
     Jenkins-Slave = "t2.micro"
  }
}

variable "mykey" {
  default = "varunAWSkey"
}

module "server" {
  for_each = var.myhosts
  servername = each.key
  type = each.value
  pemfile = var.mykey
  source = "./modules/instances"
}
