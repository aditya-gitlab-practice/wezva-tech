provider "aws" {
  region = "ap-south-1"
}

#=====================================================
#VPC creation ( private )
#=====================================================

resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  
  tags = {
    Name = "VPC - wezva"
  }
}

data "aws_availability_zones" "all" {}

#=====================================================
#public subnet creation
#=====================================================

resource "aws_subnet" "public_ap_south_1a" {
  vpc_id = aws_vpc.my_vpc.id
  availability_zone = data.aws_availability_zones.all.names[0]
  cidr_block = var.subnet1_cidr
  
  tags = {
    Name = "Public Subnet - wezva"
  }
}

#=====================================================
#internet gateway for traffic from internet (public)
#=====================================================
resource "aws_internet_gateway" "my_vpc_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "DEMO IGW - wezva"
  }
}

#=====================================================
#creation of route table for public subnet
#=====================================================
resource "aws_route_table" "my_vpc_public" {
  vpc_id = aws_vpc.my_vpc.id
  
  route {
    cidr_block = "0.0.0.0/0"
	gateway_id = aws_internet_gateway.my_vpc_igw.id

    }
    
        tags = {
        Name = "DEMO Public RouteTable - wezva"

}

#=====================================================
#mapping route table to subnet
#=====================================================

resource "aws_route_table_association" "my_vpc_ap_south_1a_public" {
  subnet_id  = aws_subnet.public_ap_south_1a.id
  route_table_id = aws_route_table.my_vpc_public.id
}

#=========================================================
#security group to define the inbound and outbound rules
#=========================================================

resource "aws_security_group" "instance" {
  name = "public_security_group"
  vpc_id = aws_vpc.my_vpc.id
  
  # Allow all outbound
  egress {
    from_port = 0
	to_port = 0
	protocol = "-1"
	cidr_blocks = ["0.0.0.0/0"]
  }
  
  # Inbound for SSH
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }	
  
  # Inbound for Web server
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }	
}

#=========================================================
#EC2 server instance creation
#=========================================================

resource "aws_instance" "server" {
  ami = var.amiid
  instance_type = var.type
  key_name = var.pemfile
  
  vpc_security_group_ids = [aws_security_group.instance.id]
  subnet_id = aws_subnet.public_ap_south_1a.id
  availability_zone = data.aws_availability_zones.all.names[0]

  associate_public_ip_address = true

  user_data = <<-EOF
               #!/bin/bash
               echo '<html><body><h1 style="font-size:50px;color:blue;">King<br> <font style="color:red;"> www.wezva.com <br> <font style="color:green;"> 123455 </h1> </body></html>' > index.html
               nohup busybox httpd -f -p 8080 &
              EOF
  
  tags = {
     Name = "Web server - weza"
  }
}  

#=========================================================
#Private subnet creation
#=========================================================

resource "aws_subnet" "private_ap_south_1b" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.subnet2_cidr
  availability_zone = data.aws_availability_zones.all.names[1]

  tags = {
    Name = "Private Subnet - wezva"
  }
}

#=========================================================
#route table creation for association with private subnet
#=========================================================

resource "aws_route_table" "my_vpc_private" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        network_interface_id = aws_instance.server.primary_network_interface_id
    }

    tags = {
        Name = "DEMO Private RouteTable - wezva"
    }
}

#=========================================================
#route table association with subnet
#=========================================================

resource "aws_route_table_association" "my_vpc_ap_south_1b_private" {
  subnet_id  = aws_subnet.private_ap_south_1b.id
  route_table_id = aws_route_table.my_vpc_private.id
}

#=========================================================
#security group for private instance
#=========================================================

resource "aws_security_group" "db" {
  name = "private_security_group"
  vpc_id = aws_vpc.my_vpc.id
  
  # Allow all outbound
  egress {
    from_port = 0
	to_port = 0
	protocol = "-1"
	cidr_blocks = ["0.0.0.0/0"]
  }
  
  # Inbound for SSH
  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    security_groups = [aws_security_group.instance.id]
  }	
}

#=========================================================
#creation of ec2 server in private subnet
#=========================================================


resource "aws_instance" "db" {
   ami           = var.amiid
   instance_type = var.type
   key_name      = var.pemfile
   vpc_security_group_ids = [aws_security_group.db.id]
   subnet_id = aws_subnet.private_ap_south_1b.id
   availability_zone = data.aws_availability_zones.all.names[1]
   
   associate_public_ip_address = true

   tags = {
       Name = "DB Server - wezva"
   }
  
}
