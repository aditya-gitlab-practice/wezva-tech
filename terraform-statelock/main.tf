provider "aws" {
  region = "ap-south-1"
  profile = "default"
}

resource "aws_instance" "example"{
  ami = "ami-08e5424edfe926b43"
  instance_type = "t2.micro"
}

resource "aws_s3_bucket" "example2"{
  bucket = "varun-bucket-august05-2023"
}

resource "aws_dynamodb_table" "example_dynamoDB"{
	name = "terraform-statelock-dynamoDB"
	hash_key = "LockID"
	read_capacity = 20
	write_capacity = 20
	attribute {
		name = "LockID"
		type = "S"
	}
	tags = {
		Name = "Dynamo DB for locking the state file"
	}
}
  
