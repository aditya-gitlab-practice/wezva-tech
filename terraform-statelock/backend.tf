terraform {
    backend "s3" {
        bucket = "varun-bucket-august05-2023"
        key = "default/example.tf"
        region = "ap-south-1"
        dynamodb_table = "terraform-statelock-dynamoDB"
        encrypt = "true"
        }
}
